### DOTS

Here are my dots for a few different programs I use regularly in my environment.

## SCREENSHOT

![screenshot](https://gitlab.com/Vancelot/dotfiles/raw/laptop/screenshot.png)

## GENERAL SOFTWARE

* i3
* zsh
* vim \(>7.4\)


# WINDOW MANAGER
- [i3](https://i3wm.org/docs/userguide.html)
	- [Rofi](https://github.com/davatorium/rofi): Program launcher
	- [Compton](https://github.com/yshui/compton): Compositor for X. Manages things like transparency shading etc.  
	- [Ranger](https://github.com/ranger/ranger): File manager/explorer but vim

# SHELL
- [zsh](http://zsh.sourceforge.net/Doc/)
	- [ohmyzsh](https://ohmyz.sh/): Framework for managing zsh config

# [VIM](https://www.vim.org/)
- Plugins
	- [Vundle](https://github.com/VundleVim/Vundle.vim): Package manager
	- [Auto-Pairs](https://github.com/jiangmiao/auto-pairs): Automatically inserts matching parens, quotes, braces
	- [NERDTree](https://github.com/scrooloose/nerdtree): File explorer
	- [Airline](https://github.com/vim-airline/vim-airline): Status line 
	- [Gitgutter](https://github.com/airblade/vim-gitgutter): Shows git diffs inline

You could probably copy these files into their appropriate locations and it might work, but I'm not familiar enough with the different distros standards to guarantee anything. You're probably best off locating or creating these config files on your system, and copying out the bits of my config you like. If you have any troubles let me know. Good luck!

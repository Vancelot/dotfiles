" All system-wide defaults are set in $VIMRUNTIME/archlinux.vim (usually just
" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'Valloric/YouCompleteMe'
Plugin 'SirVer/ultisnips'
Plugin 'lervag/vimtex'
Plugin 'Yggdroot/indentLine'
Plugin 'jiangmiao/auto-pairs'
Plugin 'kien/ctrlp.vim'
Plugin 'OmniSharp/omnisharp-vim'
Plugin 'tpope/vim-dispatch'
Plugin 'lifepillar/vim-solarized8'
Plugin 'morhetz/gruvbox'
Plugin 'airblade/vim-gitgutter'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'mattn/emmet-vim'
Plugin 'sirtaj/vim-openscad'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

set mouse=a
set number
set autoindent
set tabstop=3
set shiftwidth=3
set clipboard=unnamedplus
set hidden
set noea
set encoding=utf-8
syntax on
"packloadall
silent! helptags ALL
filetype plugin on

:let mapleader = ","
nnoremap <silent> ff :bn<CR>
nnoremap <silent> FF :tabn<CR>
nnoremap <silent> tt :bp<CR>
nnoremap <silent> TT :tabp<CR>
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <leader>ev :edit ~/.vimrc<CR>
nnoremap <leader>sv :source ~/.vimrc<CR>
nnoremap <leader>eb :edit ~/.bashrc<CR>
nnoremap <leader>q :bp<bar>sp<bar>bn<bar>bd<CR>
nnoremap <silent> <leader>f :lnext<CR>
nnoremap <silent> <leader>t :lprev<CR>
nnoremap <silent> <leader>u <C-U>
nnoremap <silent> <leader>d <C-D>
nnoremap <silent> <leader>w <C-W>
nnoremap <silent> <leader>k <C-W>k
nnoremap <silent> <leader>j <C-W>j
nnoremap <silent> <leader>h <C-W>h
nnoremap <silent> <leader>l <C-W>l

inoremap jk <Esc>
inoremap JK <Esc>
inoremap Jk <Esc>

augroup myFileTypes

	"AutoPairs
		au FileType ruby
			\ let b:AutoPairs = AutoPairsDefine({'\v(^|\W)\zsdo': 'end//n'}) |
			\ setlocal tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab textwidth=120 |
			\ set filetype=ruby.chef |
			\ highlight ExtraWhitespace ctermbg=red guibg=red |
			\ au Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\\t/

		au FileType yaml setlocal tabstop=2 expandtab

		au FileType haskell setlocal tabstop=2 expandtab

		"Latex
		au FileType tex let maplocalleader = '\'

		au FileType text setlocal spell textwidth=75

		au FileType asm
				\ setlocal ft=masm tabstop=10 |
				\ let b:ycm_auto_trigger=0

		au FileType html,css EmmetInstall

		au FileType rst setlocal textwidth=120
augroup END




set listchars=tab:\|\ 
set list
let g:indentLine_char='|'
"let g:indentLine_char_list = ['|', '¦', '┆', '┊']

"YouCompleteMe ycm
"let g:ycm_path_to_python_interpreter = '/usr/bin/python2'
let g:ycm_global_ycm_extra_conf = '/usr/share/vim/vimfiles/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
"let g:ycm_rust_src_path = '~/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src'
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:EclimCompletionMethod = 'omnifunc'
let g:ycm_complete_in_comments = 1
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1

"Ultisnips ultisnips
nnoremap <silent> <leader>es :UltiSnipsEdit<CR>
let g:UltiSnipsSnippetDirectories=["~/.vim/UltiSnips"]
let g:UltiSnipsExpandTrigger='<c-j>'
let g:UltiSnipsJumpForwardTrigger='<c-j>'
let g:UltiSnipsJumpBackwardTrigger='<c-k>'
let g:UltiSnipsListSnippets='<c-h>'

"Latex
let g:vimtex_view_general_viewer = 'evince'
if !exists('g:ycm_semantic_triggers')
  let g:ycm_semantic_triggers = {}
endif
let g:ycm_semantic_triggers.tex = g:vimtex#re#youcompleteme

"Omnisharp
let g:OmniSharp_start_server = 0
let g:OmniSharp_server_path = '~/.omnisharp/omnisharp/omnisharp/OmniSharp.exe'

"CtrlP
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

"Colorscheme (Solarized Gruvbox)
"set background=dark
"colorscheme solarized8_flat
let g:gruvbox_contrast_dark = 'medium'
colorscheme gruvbox

"Emmet HTML/CSS
let g:user_emmet_install_global = 0
let g:user_emmet_settings = {
\	'html' : {
\		'indentation' : '	'
\	},
\}

"Nerdtree
let g:NERDTreeWinSize=15

"Indentation
hi SpecialKey ctermfg=DarkGray ctermbg=none cterm=none
hi SpecialKey guifg=DarkGray guibg=Red

"Airline
"let g:airline_solarized_bg='dark'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline_powerline_fonts=1
let g:airline_theme='gruvbox'
